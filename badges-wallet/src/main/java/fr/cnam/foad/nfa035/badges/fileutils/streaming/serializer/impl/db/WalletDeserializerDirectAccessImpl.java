package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class WalletDeserializerDirectAccessImpl implements DirectAccessDatabaseDeserializer {

    private static final Logger LOG = LogManager.getLogger(WalletDeserializerDirectAccessImpl.class);

    private OutputStream sourceOutputStream;
    //List<DigitalBadgeMetadata> metas;
    Set<DigitalBadge> metas;

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     * @param metas les métadonnées du wallet, si besoin
     */
    public WalletDeserializerDirectAccessImpl(OutputStream sourceOutputStream, Set<DigitalBadge> metas) { // changement en HashSet
        this.setSourceOutputStream(sourceOutputStream);
        //this.metas = metas;
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @throws IOException
     */
    //@Override
    public void deserialize(WalletFrameMedia media, DigitalBadgeMetadata meta) throws IOException {

        long pos = meta.getWalletPosition();
        media.getChannel().seek(pos);
        // Lecture de la ligne et parsage des différents champs contenus dans la ligne, de 2 façons:
        // 1/ FACILE: String[] data = media.getChannel().readLine().split(";");
        // 2/ Optimisé par le fait de passer par un BufferedReader,
        // mais Attention aux évolutions plus pertinentes à venir, éventuellement:
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
           getDeserializingStream(data[3]).transferTo(os);
        }
    }

    /**
     * {@inheritDoc}
     * @return OutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }


    /**
     * {@inheritDoc}
     *
     * Inutile => Non pris encharge par cet Objet
     *
     * @param media
     * @throws IOException
     */
    //@Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Inutile
        throw new IOException("Non pris encharge par cet Objet");
    }

    /**
     * {@inheritDoc}
     *
     * @param data
     * @return
     * @throws IOException
     */
    //@Override inutile
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     *
     * @param media
     * @param meta
     * @throws IOException
     */
    //@Override inutile
    public void deserialize(WalletFrameMedia media, DigitalBadge meta) throws IOException { //changement de paramètre DigitalBadge > DigitalBadgeMetadata
        // TODO: 27/11/2022 prototype corps de méthode
        long pos = meta.getMetadata().getWalletPosition(); // objet meta de DigitalBadge > GetMetaData de DigitalBade > getWalletPosition de DigitalBadgeMetadata
        media.getChannel().seek(pos);
        // Lecture de la ligne et parsage des différents champs contenus dans la ligne, de 2 façons:
        // 1/ FACILE: String[] data = media.getChannel().readLine().split(";");
        // 2/ Optimisé par le fait de passer par un BufferedReader,
        // mais Attention aux évolutions plus pertinentes à venir, éventuellement:
        String[] data = media.getEncodedImageReader(false).readLine().split(";");
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(data[3]).transferTo(os);
        }
    }

    /**
     * Utile pour récupérer un Flux de lecture de la source à sérialiser
     *
     * @param source
     * @return
     * @throws IOException
     */
    //@Override inutile
    public <K extends InputStream> K getSourceInputStream(DigitalBadge source) throws IOException {
        // TODO: 27/11/2022 corps de méthode ?
        return getSourceInputStream(source);
    }

    /**
     * Permet de récupérer le flux d'écriture et de sérialisation vers le media
     *
     * @param media
     * @return
     * @throws IOException
     */
    //@Override inutile
    public <T extends OutputStream> T getSerializingStream(WalletFrameMedia media) throws IOException {
        // TODO: 27/11/2022 corps de méthode ?
        return getSerializingStream(media);
    }
}
